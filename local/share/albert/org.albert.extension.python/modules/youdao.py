# -*- coding: utf-8 -*-

"""Translate text using Youdao Translate API.
Usage: yd <text>
Example: yd hello
The example works for python 3.5
windows platform: wox
Link source: https://my.oschina.net/u/1044667/blog/1622117 register an account to get api access
From mark
            with urllib.request.urlopen(req) as response:
                items = []  # 待返回的列表
                data = json.load(response)
"""

from albertv0 import *
import json
import urllib.request
import urllib.parse
import hashlib
import locale
import sys

__iid__ = "PythonInterface/v0.1"
__prettyname__ = "Youdao Translate"
__version__ = "0.1"
__trigger__ = "yd "  # 触发命令
__author__ = "mark"
__dependencies__ = []

ua = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36"
urltmpl = "http://fanyi.youdao.com/openapi.do?keyfrom=SeekBetterMe&key=164530784&type=data&doctype=json&version=1.1&q=%s"

iconPath = iconLookup('config-language')  # 自定义图标
if not iconPath:
    iconPath = ":python_module"

def handleQuery(query):
    if query.isTriggered:
        def getItem(text, subtext=''):  # 显示的项
            item = Item(
                id=__prettyname__, 
                icon=iconPath, 
                completion=query.rawString,
                text=text, 
                subtext=subtext
            )
            item.addAction(ClipAction("Copy translation to clipboard", text))  # 项支持的操作
            return item
        txt = query.string.strip()
        if txt:
            url = urltmpl % ( urllib.parse.quote_plus(txt) )
            req = urllib.request.Request(url, headers={'User-Agent': ua})
            with urllib.request.urlopen(req) as response:
                items = []  # 待返回的列表
                str_response = response.read().decode('utf-8')
                data = json.loads(str_response)
                if 'basic' in data:
                    if 'phonetic' in data['basic']:  # 读音
                        items.append(getItem('/' + data['basic']['phonetic'] + '/'))
                    for exp in data['basic']['explains']:  # 释义
                        items.append(getItem(exp, 'basic'))
                elif 'translation' in data:  # 句子翻译
                    items.append(getItem(data['translation'][0], 'translation'))
                if 'web' in data:  # 网络释义
                    for w in data['web']:
                        value = list(set(w['value']))  # 去重
                        items.append(getItem(w['key']+': '+'; '.join(value[:2]), 'web'))
                return items
        else:
            return getItem("Enter a translation query")
