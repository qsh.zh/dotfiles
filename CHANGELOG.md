# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).


## [0.0.4] - 2021-MM-DD

### Added

- zsh `bindkey` in `shell/aliases`
- zsh history setting in `zsh/settings.zsh`
- CHANGELOG.md
- alias `act` and `$tact`, help quick jump to active project folder
- add `dall` in `shell_local_after`, guarantee used updated data everytime. It may introduce troubles when not in local branch.
- add `logrun` and `nlogrun` for running cmd and pipe its output to file and screen

### Changed

- [forgit](https://github.com/wfxr/forgit) support git interactively. `ga,glo,gi,gd,grh,gcf,gcp,grb`
- [delta](https://github.com/dandavison/delta) *git diff*, *git show*, *git log -p*, *git stash show -p* provide colorful view
- `udot` only supports merge current branch into dev instead of using `local` by default

### Deprecated

### Removed

### Fixed

- For ipynb, parent dir auto included in path.

