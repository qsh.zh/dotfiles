"--------------------
" Python BufRead need a space after comma, strange design
"--------------------
au BufNewFile, BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix
    \ let python_highlight_all=1
    \ syntax on

autocmd BufWritePre *.py execute ':Black'
execute "set <F3>=\eOR"
noremap <F3> :Black<CR>

