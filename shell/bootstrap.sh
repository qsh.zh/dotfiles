path_prepend "$HOME/.local/bin"
path_prepend "$HOME/.zqs_bin"
path_prepend "$HOME/.dotfiles/bin"
path_prepend "$HOME/jam/bin"
path_prepend "$HOME/usr/local/bin"
path_prepend "$HOME/usr/bin"
GEM_HOME="$HOME/.gems"
path_prepend "$HOME/.gems/bin"
if [ -d /opt/homebrew/bin ]; then
    path_prepend "/opt/homebrew/sbin"
    path_prepend "/opt/homebrew/bin"
    path_append "/opt/homebrew/opt/coreutils/libexec/gnubin"
fi


export RED='\033[0;31m'
export GREEN='\033[0;32m'
export BLUE='\033[0;34m'
export PURPLE='\033[0;35m'
export CYAN='\033[0;36m'
export NC='\033[0m' # No Color
export CALERT=$RED
export CINFO=$CYAN

function TInfo() {
    printf "${CYAN} $1 ${NC}\n"
}
function TAlert() {
    printf "${RED} $1 ${NC}\n"
}