
alias qinsheng="source $SHPATH/modules/_python.sh"
alias ipy="qinsheng && ipython"
alias sys-dev="conda deactivate; source $rc_file"
# TODO: move _pyhon module into a better place! Maybe enable conda env is reasonable
alias psh="poetry shell"


source $SHPATH/modules/ubuntu.sh
source $SHPATH/modules/cd.sh
source $SHPATH/modules/_docker.sh
source $SHPATH/modules/_ngc.sh
source $SHPATH/modules/git.sh
source $SHPATH/modules/cheat.sh
source $SHPATH/modules/latex.sh
source $SHPATH/modules/ros.sh

