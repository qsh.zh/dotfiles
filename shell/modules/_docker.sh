alias rdk="docker -H ssh://qzhang419@128.61.184.215"
alias sdk="ssh -NL localhost:23750:/var/run/docker.sock gpu"


XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth

# FIX: https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if [ -n ${DISPLAY+x} ];then
    if [ -e "$XAUTH" ]; then
        echo "Already exist $XAUTH"
    else
        touch $XAUTH
        xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
    fi
fi

alias jamdk-run="docker run -d \
        --shm-size 64G\
        --gpus all\
        --network host\
        --volume=$XSOCK:$XSOCK:rw --privileged\
        --volume=$XAUTH:$XAUTH:rw \
        --volume=$HOME/.ssh:$HOME/.ssh:rw \
        --volume=$HOME/jam:$HOME/jam:rw \
        --env=\"XAUTHORITY=${XAUTH}\" \
        --env=\"DISPLAY=$DISPLAY\" \
        "
unset XSOCK
unset XAUTH

function dkbash() {
    if [ "$#" -eq 0 ]; then
        container=$(docker ps -q | awk 'FNR == 1 {print $1;}')
        docker exec -it $container bash
    else
        # for input a number case
        if [[ $1 == ?(-)+([0-9]) ]]; then
            # it is a trick: see https://stackoverflow.com/questions/13799789/expansion-of-variable-inside-single-quotes-in-a-command-in-bash
            container=$(docker ps -q | awk 'FNR == '"$1"' {print $1;} ')
            docker ps | grep $container
            docker exec -it $container bash
        # for input string case
        else
            docker exec -it $1 bash
        fi
    fi
}

############################################ fzf docker
if ! type _extra_fzf_completions_timeout >/dev/null 2>&1; then
    _extra_fzf_completions_timeout() {
        local timeout_interval=${EXTRA_FZF_COMPLETIONS_TIMEOUT:-7}
        timeout $timeout_interval $@
    }
fi

_fzf_complete_docker_run_post() {
    awk '{print $1":"$2}'
}

_fzf_complete_docker_run () {
    _fzf_complete "$EXTRA_FZF_COMPLETIONS_FZF_PREFIX -m --header-lines=1" "$@" < <(
        _extra_fzf_completions_timeout docker images
    )
}

_fzf_complete_docker_common_post() {
    awk -F"\t" '{print $1}'
}

_fzf_complete_docker_common () {
    _fzf_complete "$EXTRA_FZF_COMPLETIONS_FZF_PREFIX --reverse -m" "$@" < <(
        _extra_fzf_completions_timeout docker images --format "{{.Repository}}:{{.Tag}}\t {{.ID}}"
    )
}

_fzf_complete_docker_container_post() {
    awk '{print $NF}'
}

_fzf_complete_docker_container () {
    _fzf_complete "$EXTRA_FZF_COMPLETIONS_FZF_PREFIX -m --header-lines=1" "$@" < <(
        _extra_fzf_completions_timeout docker ps -a
    )
}

_fzf_complete_docker() {
    local words
    setopt localoptions noshwordsplit noksh_arrays noposixbuiltins
    # http://zsh.sourceforge.net/FAQ/zshfaq03.html
    # http://zsh.sourceforge.net/Doc/Release/Expansion.html#Parameter-Expansion-Flags
    words=(${(z)LBUFFER})
    local stop_iter=${#words[@]}
    local beg_iter=1
    for ((i=$beg_iter; i<$stop_iter;i++)); do
        case "${words[$i]}" in
            run)
                _fzf_complete_docker_run "$@"
                return
            ;;
            exec|rm)
                _fzf_complete_docker_container "$@"
                return
            ;;
            save|load|push|pull|tag|rmi)
                _fzf_complete_docker_common "$@"
                return
            ;;
        esac
    done

}
