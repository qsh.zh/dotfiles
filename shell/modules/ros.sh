#############################ROS###############################################
alias cw='cd ~/catkin_ws'
alias cs='cd ~/catkin_ws/src'
alias cm='cd ~/catkin_ws && catkin_make'
function ros_() {
    # source /opt/ros/melodic/setup.bash
    export EDITOR='code'
    # export ROS_MASTER_URI=http://128.61.50.202:11311
    # export ROS_HOSTNAME=128.61.50.202
    source /opt/ros/melodic/setup.zsh
    source ~/catkin_ws/devel/setup.zsh
    export TURTLEBOT3_MODEL=burger
    export ROS_MASTER_URI=http://localhost:11311
    export ROS_HOSTNAME=localhost
    alias test="roslaunch Zhang_Qinsheng run.launch"
    alias riv="rviz -d $(rospack find turtlebot3_navigation)/rviz/turtlebot3_navigation.rviz"
    # alias vie="roslaunch rviz -d turtlebot3_navigation/rviz/turtlebot3_navigation.rviz"
}
alias ros="ros_"

function burger_() {
    export EDITOR='code'
    export ROS_MASTER_URI=http://143.215.105.38:11311
    # export ROS_MASTER_URI=http://128.61.57.60:11311
    export ROS_HOSTNAME=128.61.50.207
    source /opt/ros/melodic/setup.bash
    source ~/catkin_ws/devel/setup.bash
    export TURTLEBOT3_MODEL=burger
    alias q="rostopic pub -1 /cmd_vel geometry_msgs/Twist -- '[0, 0.0, 0.0]' '[0.0, 0.0, 0]'"
    alias test="roslaunch Zhang_Qinsheng run.launch"
    alias k="roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch"
    alias riv="rviz -d $(rospack find turtlebot3_navigation)/rviz/turtlebot3_navigation.rviz"
    alias scpfinal="scp -r ~/catkin_ws/src/final burger:~/catkin_ws/src/"
    alias scpbag="scp -r burger:~/Document/rosbag/ ~/Desktop"
    alias scpbash="scp ~/Documents/cs7785/.bashrc burger:~/"

}
alias bger="burger_"

function kuka() {
    # source /opt/ros/melodic/setup.bash
    export EDITOR='code'
    export ROS_HOSTNAME=192.168.10.13
    export ROS_MASTER_URI=http://${ROS_HOSTNAME}:11311
    source /opt/ros/$(ls /opt/ros)/setup.zsh
}