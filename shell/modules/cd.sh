alias j~="cd $HOME"
alias jd="cd $HOME/Downloads"
alias jdb="cd $HOME/Dropbox\ \(GaTech\)"
alias jp="cd $HOME/projects"
alias o~="open $HOME"
alias od="open $HOME/Downloads"
alias oi="open $HOME/Pictures"


cdd() {
    local dir
    dir=$(find ${1:-.} -type d 2>/dev/null | fzf +m) && cd "$dir"
}

cdf() {
    local file
    local dir
    file=$(fzf +m -q "$1") && dir=$(dirname "$file") && cd "$dir"
}


# cf - fuzzy cd from anywhere
# ex: cf word1 word2 ... (even part of a file name)
# zsh autoload function
cf() {
    local file

    file="$(locate -Ai -0 $@ | grep -z -vE '~$' | fzf --read0 -0 -1)"

    if [[ -n $file ]]; then
        if [[ -d $file ]]; then
            cd -- $file
        else
            cd -- ${file:h}
        fi
    fi
}


vcdf() {
    vim $(fzf +m -q "$1")
}
ccdf() {
    code $(fzf +m -q "$1")
}


vg() {
    local file
    local line

    read -r file line <<<"$(ag --nobreak --noheading $@ | fzf -0 -1 | awk -F: '{print $1, $2}')"

    if [[ -n $file ]]; then
        vim $file +$line
    fi
}

fe() (
    IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
    [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
)

cdgit() {
    CURRENT_DIR=$PWD
    while [[ "$CURRENT_DIR" != "/" && "$CURRENT_DIR" != "$HOME" && ! -d "$CURRENT_DIR/.git" ]]; do
        CURRENT_DIR=$(dirname "$CURRENT_DIR")
    done
    if [ -d "$CURRENT_DIR/.git" ]; then
        cd "$CURRENT_DIR"
    else
        echo "No git directory found up to home directory."
    fi
}